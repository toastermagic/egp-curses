import os
import json
import curses
from time import sleep
import random
import zipfile


class AnimChar:
    def __init__(self, char, target_x, target_y, pad):
        self.char = char
        self.target_x = target_x
        self.target_y = target_y
        self.last_x = 0
        self.last_y = 0
        self.pad = pad

    def randomize(self, width, height):
        self.x = random.randint(1, width - 2)
        self.y = random.randint(1, height - 2)
        self.delta_x = self.target_x - self.x
        self.delta_y = self.target_y - self.y

    def draw(self, x, y, char):
        try:
            self.pad.addch(y, x, char)
        except:
            print(f"Error drawing {char} at {x}, {y}")

    def clear(self):
        self.draw(int(self.last_x), int(self.last_y), " ")

    def tick(self, increment):
        x = self.x + self.delta_x * increment
        y = self.y + self.delta_y * increment
        self.draw(int(x), int(y), self.char)
        self.last_x = x
        self.last_y = y


def open_json():
    with open("frames.zip", "rb") as frames_zip:
        with zipfile.ZipFile(frames_zip, "r") as zip_ref:
            first = zip_ref.infolist()[0]
            with zip_ref.open(first, "r") as fo:
                return json.load(fo)


def get_max_lines(text_obj):
    return max([len(x["lines"]) for x in text_obj])


def get_frame_chars(pad, frame):
    y = 0
    for line in frame["lines"]:
        x = 0
        y += 1
        for char in line:
            x += 1
            if char == " ":
                continue
            yield AnimChar(char, x, y, pad)


def main(stdscr):
    curses.use_default_colors()
    frames = open_json()
    height = get_max_lines(frames) - 1
    width = len(frames[0]["lines"][0]) - 1

    pad = curses.newpad(height * 2, width)
    chars = list(get_frame_chars(pad, frames[0]))

    anim_time = 1
    anim_frame_count = 60
    anim_frame_time = anim_time / anim_frame_count

    for char in chars:
        char.randomize(width, height)
        char.tick(0)
        pad.refresh(0, 0, 0, 0, curses.LINES - 1, curses.COLS - 1)
        sleep(0.01)

    for i in range(anim_frame_count + 1):
        for char in chars:
            char.clear()
        for char in chars:
            char.tick(i * anim_frame_time)
        pad.refresh(0, 0, 0, 0, curses.LINES - 1, curses.COLS - 1)
        sleep(anim_frame_time)

    sleep(2)

    for f in frames:
        frame(pad, 0, 0, f, 0.015)

    frame(pad, 0, 0, frames[0], 0)
    sleep(60)


def frame(pad, y, x, frame, interval):
    row = y
    for line in frame["lines"]:
        pad.addstr(row, x, line)
        row += 1
    pad.refresh(0, 0, 0, 0, curses.LINES - 1, curses.COLS - 1)
    sleep(interval)


if __name__ == "__main__":
    os.system("clear")
    curses.wrapper(main)
